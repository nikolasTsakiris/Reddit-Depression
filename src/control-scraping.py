# -*- coding: utf-8 -*-

# imports

import praw
import pandas as pd
import numpy as np
from main import *
from csv import writer

# -------------------------- create the control group --------------------------

# extract data via multiple queries from all subreddits
post_titles_history = [] 
history = reddit.subreddit('history').hot(limit=220)
books = reddit.subreddit('books').hot(limit=220)
writing = reddit.subreddit('writing').hot(limit=220)

def append_and_create(titles, posts):# extract and create the dataset
    for post in posts: 
        titles.append([post.title, post.id, post.author, post.created_utc, 'no'])
        
    df = pd.DataFrame(titles, columns=['post title',
                                                    'post_id',
                                                    'post_author',
                                                    'post.created_utc',
                                                    'has-depression'])
    # convert unix time to time
    df['datetime'] = pd.to_datetime(df_all['post.created_utc'], unit='s').dt.time
    # --------------------------       --------------------------
    df.to_csv('../datasets/reddit_members.csv', header=False, index=False, mode='a')
        
append_and_create(post_titles_history, history)
append_and_create(post_titles_history, books) 
append_and_create(post_titles_history, writing)



    
    
