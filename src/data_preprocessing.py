# -*- coding: utf-8 -*-

# imports
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.cluster.hierarchy as sch
import main
from sklearn.cluster import AgglomerativeClustering
import csv

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.model_selection import cross_val_score

# ----------------------------------------------------------------------------

# import the dataset
dataset = pd.read_csv('../datasets/reddit_members.csv')
dataset = dataset.dropna() # drop rows with NaN values 
# label no/yes as 0/1
dataset['has-depression'] = dataset['has-depression'].map({'yes': 1, 'no': 0})
dataset_time = dataset.sample(frac=1)
df = dataset[['datetime', 'has-depression']]

# get the values
X = df.iloc[:,0].values
y = df.iloc[:,1].values

for i in range(len(X)):
    X[i] = X[i].replace(':', '') # remove colons from time
    X[i] = float(X[i]) # turn str to float
    X[i] /= 100 # keep only hours and minutes 
    X[i] = int(X[i]) # remove digits
    
# plots
"""
The following plots try to give an insight into the timeframes of which 
the subjects tend to submit content
The results show that depressed individuals tend to submit posts near the extremes,
with much bigger gaps, meaning that the prefered timeframes range from 03.00-05.00 and 16.00-24.00.
Note that the depressed individuals are quantitavely less than the non-depressed ones.
The non-depressed subjects tend to submit in an evenly distributed manner around the clock,
with a mild decrease around the ranges of 08.30-10.00 and 16.00-18.00.
"""
# run the plots seperately
plt.scatter(X[:50],y[:50], c='magenta')
plt.scatter(X[51:100], y[51:100], c='green')
plt.scatter(X[101:150], y[101:150], c='red')
plt.scatter(X[151:200], y[151:200], c='brown')
plt.scatter(X[201:250], y[201:250], c='fuchsia')
plt.scatter(X[251:300], y[251:300], c='black')
plt.scatter(X[301:350], y[301:350], c='magenta')
plt.scatter(X[351:400], y[351:400], c='brown')

# -----------------------------------------------------------------------------
# get the word count for each one
depressed_titles = []
depressed_count = []
non_titles = []
non_count = []
index = 0

for title in range(196):
        depressed_titles.append(dataset.iloc[title, 0])
        res = len(depressed_titles[title].split())
        depressed_count.append(res)
        
for title in range(196,880):
        non_titles.append(dataset.iloc[title, 0])
        res = len(non_titles[index].split())
        non_count.append(res)
        index += 1


# ---------------------- hierarchical clustering ------------------------------

# prepare the arrays
x1 = X.copy()
depressed_count = np.asarray(depressed_count)
non_count = np.asarray(non_count)
affirmative = np.column_stack((x1[0:196], depressed_count))
negative = np.column_stack((x1[196:], non_count))

# training and predictions for depressed individuals
hc = AgglomerativeClustering(n_clusters=6, affinity='euclidean', linkage='ward')
y_hc = hc.fit_predict(affirmative)

# plot the the algorithm for depressed individuals
plt.scatter(affirmative[y_hc==0,0], affirmative[y_hc==0,1], s=100, c='red', label='Cluster - 1')
plt.scatter(affirmative[y_hc==1,0], affirmative[y_hc==1,1], s=100, c='blue', label='Cluster - 2')
plt.scatter(affirmative[y_hc==2,0], affirmative[y_hc==2,1], s=100, c='magenta', label='Cluster - 3')
plt.scatter(affirmative[y_hc==3,0], affirmative[y_hc==3,1], s=100, c='cyan', label='Cluster - 4')
plt.scatter(affirmative[y_hc==4,0], affirmative[y_hc==4,1], s=100, c='brown', label='Cluster - 5')
plt.scatter(affirmative[y_hc==4,0], affirmative[y_hc==4,1], s=100, c='black', label='Cluster - 6')
plt.title('agglomerative clustering for depressed individuals')
plt.xlabel('time(*100)')
plt.ylabel('word count')
plt.legend()
plt.show()

# training and predictions for non - depressed individuals
y_hc_NE = hc.fit_predict(negative)

# plot the the algorithm for non - depressed individuals
plt.scatter(negative[y_hc_NE==0,0], negative[y_hc_NE==0,1], s=100, c='red', label='Cluster - 1')
plt.scatter(negative[y_hc_NE==1,0], negative[y_hc_NE==1,1], s=100, c='blue', label='Cluster - 2')
plt.scatter(negative[y_hc_NE==2,0], negative[y_hc_NE==2,1], s=100, c='magenta', label='Cluster - 3')
plt.scatter(negative[y_hc_NE==3,0], negative[y_hc_NE==3,1], s=100, c='cyan', label='Cluster - 4')
plt.scatter(affirmative[y_hc==4,0], affirmative[y_hc==4,1], s=100, c='brown', label='Cluster - 5')
plt.scatter(affirmative[y_hc==5,0], affirmative[y_hc==5,1], s=100, c='black', label='Cluster - 6')
plt.title('agglomerative clustering for non - depressed individuals')
plt.xlabel('time(*100)')
plt.ylabel('word count')
plt.legend()
plt.show()


# -----------------EXTRACT SUBMISSIONS OF ALL AUTHORS-------------------------

# get the list of depressed redditor names
depressed_authors = dataset.iloc[0:196,2].values
non_depressed_authors = dataset.iloc[196:,2].values

# create the lists
depAuthTi = []
nonAuthTi = []

# for loop to get all the submissions of the depressed subjects
for author in depressed_authors:
    auth = main.reddit.redditor(author)
    submissions = auth.submissions.top('all')
    for submission in submissions:
        depAuthTi.append([submission.id, submission.title, submission.created_utc])
        
# for loop to get all the submissions of the non-depressed subjects
for author in non_depressed_authors:
    auth = main.reddit.redditor(author)
    submissions = auth.submissions.top('all')
    for submission in submissions:
        nonAuthTi.append([submission.id, submission.title, submission.created_utc])
        
# to csv
affirmativeDF = pd.DataFrame(depAuthTi, columns=['submission id',
                                                    'submission title',
                                                    'created utc',])

negativeDF = pd.DataFrame(nonAuthTi, columns=['submission id',
                                                    'submission title',
                                                    'created utc',])
# convert unix time to time
affirmativeDF['datetime'] = pd.to_datetime(affirmativeDF['created utc'], unit='s').dt.time
negativeDF['datetime'] = pd.to_datetime(negativeDF['created utc'], unit='s').dt.time
# distinguish diagnose
affirmativeDF['diagnose'] = 1
negativeDF['diagnose'] = 0
# create the .csv files
affirmativeDF.to_csv('../datasets/affirmativeDF.csv', header=True, index=True, mode='a')
negativeDF.to_csv('../datasets/negativeDF.csv', header=True, index=True, mode='a')
#combine the dataframes
merged = pd.concat([affirmativeDF, negativeDF])
merged = merged.sample(frac=1) # shuffle the rows
merged = merged.dropna() # drop rows with NaN values 
merged.to_csv('../datasets/final.csv', header=True, index=True, mode='a')

# conversion to tsv
final = pd.read_csv('../datasets/final.csv')
final = final.drop(columns='Unnamed: 0')

X = final.iloc[:,[1,4]].values
temp = pd.DataFrame(X, columns=['text', 'diagnose'])
temp.to_csv('../datasets/temp_before_tsv.csv')

# convert to tsv
csv.writer(open("../datasets/final.tsv", 'w+'), delimiter='\t').writerows(csv.reader(open("../datasets/temp_before_tsv.csv")))




    



















