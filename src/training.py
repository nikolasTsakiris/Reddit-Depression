# -*- coding: utf-8 -*-

# imports
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import main
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.model_selection import cross_val_score

# functions 
def unix_to_normal(X):
    for i in range(len(X)):
        X[i,1] = X[i,1].replace(':', '') # remove colons from time
        X[i,1] = float(X[i,1]) # turn str to float
        X[i,1] /= 100 # keep only hours and minutes 
        X[i,1] = int(X[i,1]) # remove digits
    return X

# -------------------------- TRAINING MODELS BELOW ------------------------
final = pd.read_csv('../datasets/final.csv')
final = final.drop(columns='Unnamed: 0')
# calculate the word count for each submission
word_count = []
for post in range(len(final)):
        res = len(final.iloc[post,1].split())
        word_count.append(res)
#insert the column into index 3
final.insert(loc=3, column='word_count', value=word_count)

X = final.iloc[:,3:5].values
y = final.iloc[:,-1].values

# turn datetime to integer
X = unix_to_normal(X)

# splitting
xtrain, xtest, ytrain, ytest = train_test_split(X, y, test_size=0.2)

# scaling
sd_x = StandardScaler()
xtrain = sd_x.fit_transform(xtrain)
xtest = sd_x.transform(xtest)



# --------------- XGBOOST ---------------
# training
xgb = XGBClassifier(gpu_id=0)
xgb.fit(xtrain, ytrain)

# predictions
ypred = xgb.predict(xtest)
cm = confusion_matrix(ytest, ypred)
print('confusion matrix: \n', cm)

print('score: \n', accuracy_score(ytest, ypred))

# k-fold
accuracies = cross_val_score(xgb, xtrain, ytrain, cv=10)
print('accuracy: {:.2f} %'.format(accuracies.mean()*100))
print('standard deviation: {:.2f} %'.format(accuracies.std()*100))
