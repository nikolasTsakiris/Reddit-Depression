# -*- coding: utf-8 -*-

# imports
import pandas as pd
import numpy as np
from main import *

# -------------------------- r/depression --------------------------


# extract data via multiple queries
post_titles = [] 
type_depressed_posts = reddit.subreddit('depression').search('i got diagnosed', limit=None)

# loop and save the findings
for post in type_depressed_posts: # containing the word 'diagnosed'
    post_titles.append([post.title, post.id, post.author, post.created_utc, 'yes'])

# pass the depressed subjects into a dataframe
df = pd.DataFrame(post_titles, columns=['post title',
                                        'post_id',
                                        'post_author',
                                        'post.created_utc',
                                        'has-depression'])
# convert unix time to time
df['datetime'] = pd.to_datetime(df['post.created_utc'], unit='s').dt.time

indexes = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,17,18,19,21,23,24,25,
                      27,28,29,30,32,33,34,35,41,66,95,100,
                      111,117,123,124,129,148,153,162,170,129,192,
                      204,225,222])

# keep the diagnosed
depressed = df.iloc[indexes]

# -------------------------- r/all --------------------------

# extract data via multiple queries from all subreddits
post_titles_all = [] 
type_all_posts = reddit.subreddit('all').search('i got diagnosed depression', limit=None)

# loop and save the findings
for post in type_all_posts: # containing the word 'diagnosed'
    post_titles_all.append([post.title, post.id, post.author, post.created_utc, 'yes'])
    
# pass the depressed subjects into a dataframe
df_all = pd.DataFrame(post_titles_all, columns=['post title',
                                                'post_id',
                                                'post_author',
                                                'post.created_utc',
                                                'has-depression'])
# convert unix time to time
df_all['datetime'] = pd.to_datetime(df_all['post.created_utc'], unit='s').dt.time

indexes_2 = np.array([0,1,2,4,5,6,7,8,9,10,11,12,13,14,15,
                        16,17,19,20,21,22,23,24,25,26,28,29,
                        32,34,35,36,39,41,42,43,44,46,49,50,
                        51,52,54,55,57,59,60,61,64,65,66,67,
                        68,69,70,71,72,73,74,75,77,78,79,80,
                        81,82,84,85,86,87,88,91,95,96,100,
                        102,103,106,107,109,110,111,112,113,
                        115,116,119,120,121,122,124,128,129,
                        131,132,133,134,135,136,137,138,141,
                        142,144,147,148,150,153,154,156,157,
                        158,159,160,161,163,165,166,167,168,
                        175,177,178,181,182,183,187,188,190,
                        191,193,194,196,200,201,203,204,205,
                        208,209,214,215,216,220,221,223,224,
                        225,230
                        ])

# --------------------------       --------------------------
# append the diagnosed of /all
depressed = depressed.append(df_all.iloc[indexes_2])

# write final dataframe to csv file
depressed.to_csv('../datasets/reddit_members.csv',
                                header=True,
                                index=False)



