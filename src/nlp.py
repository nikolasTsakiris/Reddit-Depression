# -*- coding: utf-8 -*-
# imports
import re
import csv
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
# import the dataset
dataset = pd.read_csv('../datasets/final.tsv', 
                      delimiter = '\t',
                      quoting = 3)

dataset = dataset.drop(columns='Unnamed: 0')

# separate sentences and targets
sentences = dataset['text'].values
targets = dataset['diagnose'].values

# splitting
sentences_train, sentences_test, ytrain, ytest = train_test_split(sentences,
                                                                  targets,
                                                                  train_size=0.8,
                                                                  random_state = 1000)

#preprocessing
tokenizer = Tokenizer(num_words=8000)
tokenizer.fit_on_texts(sentences_train)

xtrain = tokenizer.texts_to_sequences(sentences_train)
xtest = tokenizer.texts_to_sequences(sentences_test)

# Adding 1 because of  reserved 0 index
vocab_size = len(tokenizer.word_index) + 1
maxlen = 64

xtrain = pad_sequences(xtrain, padding='post', maxlen=maxlen)
xtest = pad_sequences(xtest, padding='post', maxlen=maxlen)

#delete nan elements
xtrain = np.delete(xtrain, np.where(np.isnan(ytrain)), 0)
ytrain = np.delete(ytrain, np.where(np.isnan(ytrain)), 0)

xtest = np.delete(xtest, np.where(np.isnan(ytest)), 0)
ytest = np.delete(ytest, np.where(np.isnan(ytest)), 0)

# training using CNN (keras)
embedding_dim = 32

model = tf.keras.models.Sequential()

model.add(tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
model.add(tf.keras.layers.Conv1D(4, 8, activation='relu'))
model.add(tf.keras.layers.GlobalMaxPooling1D())
model.add(tf.keras.layers.Dense(4, activation='relu'))
model.add(tf.keras.layers.Dense(4, activation='relu'))
model.add(tf.keras.layers.Dense(1, activation='sigmoid'))

"""training"""
opt = tf.keras.optimizers.Adam(learning_rate=0.00001)
#compiling
model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])

#training
model.fit(xtrain, ytrain, validation_data=(xtest, ytest), epochs=500, batch_size=128)
# evaluate
loss, acc = model.evaluate(xtest, ytest, verbose=0)
print('Test Accuracy: %f' % (acc*100))
    
# -----------------------------------------------------------------------


