# Reddit-Depression
## Reddit Users Language Processing
###### This project's goal is to identify linguistic patterns that lead to depression of reddit users.
###### Users are separated into two groups: [Manually] Verified depressed individuals, and individuals with no apparent depression manifested.
###### Timeframes of users are plotted in order to expand into their chronological habits
###### Hierarchical Clustering proved useful in order to visualise the sub-groups and their respective differentiation in (possibly subconscious) time preference to submit content.
###### ---------------------------------------------------------------
###### XGBoost was used in order to train the first model. test size = 0.2. features = [wordCount, datetime], label = diagnose, metrics = [confusion matrix, accuracy, standard deviation]
###### ---------------------------------------------------------------
###### CNN use with parameters: [filter:4, kernel_size:8, MaxPooling, {2 hidden layers, 4 neurons each, activation: relu}, {output layer with 1 neuron, activation: sigmoid}, learning_rate: 0.00001, batch_size: 128, epochs: 500]
###### number of tokenized words: 8000
###### VALIDATION ACCURACY achieved: 68.468469
#### Tools used:
- praw (Reddit API, web scraping)
- numpy
- matplotlib
- pandas
- sklearn
- tensorflow
- XGBoost
### Endgame: 
1. Sentiment analysis based on subject's various post features. 
2. Classifiers trying to accurately diagnose depression based on post-title language. 
